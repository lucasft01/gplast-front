#!/bin/bash
domains=( "simuladoragplast.ga" )
data_path="/etc/letsencrypt"
email="lucasft_01@outlook.com"

sudo su
apt-get update
apt-get install docker.io -y
apt-get install docker-compose -y

mkdir -m 755 "$data_path"

docker run -v "$data_path":/etc/letsencrypt \
-e http_proxy=$http_proxy \
-e domains="$domains" \
-e email="$email" \
-p 80:80 -p 443:443 \
--rm pierreprinetti/certbot:latest

docker-compose up -d 