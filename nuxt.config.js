module.exports = {
  /*
  ** Build configuration
  */
  build: {
    vendor: ['axios', 'vuetify']
  },
  /*
  ** Headers
  ** Common headers are already provided by @nuxtjs/pwa preset
  */
  head: {
    title: 'AGPlast',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'name', name: 'name', content: 'AGPlast' },
      { hid: 'description', name: 'description', content: 'Simulador da capacidade de manutenção' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.5.0/css/all.css' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Customize app manifest
  */
  manifest: {
    theme_color: '#3B8070',
    name: 'AGPlast',
    author: 'Lucas e Rafael',
    description: 'Simulador da capacidade de manutenção'
  },
  /*
  ** Modules
  */
  modules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/pwa',
    'nuxt-fontawesome',
    '@nuxtjs/axios'
  ],
  axios: {
    baseURL: process.env.BASE_URL || 'https://simuladoragplast.ga/api/',
    browserBaseURL: process.env.BASE_URL_BROWSER || process.env.BASE_URL || 'https://simuladoragplast.ga/api/'
  }
}
